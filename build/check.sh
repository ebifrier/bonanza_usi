#!/bin/sh

function execute_mate() {
  result=`./bonanza_usi_x64.exe << EOF
usi
setoption name Threads value 4
setoption name Memory[MB] value 768
isready
position $1
go mate 10000
EOF
  `
  
  echo ${result#*checkmate }
}

filename="tsume.sfen"
cat ${filename} | while read line
do
  IFS_ORIGINAL="$IFS"
  IFS=,
  values=($line)
  IFS="$IFS_ORIGINAL"
  
  echo "${values[0]}"
  result=`execute_mate "${values[1]}"`
  if [ ! x"$result" = x"${values[2]}" ]; then
    echo "error"
  fi
done
